%define EXIT 60
section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    inc rax
    cmp byte[rdi+rax], 0
    jne .loop
.return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rcx, 10
    mov rax, rdi
    mov rbx, rsp
    dec rsp
    .loop:
     xor rdx, rdx
     div rcx
     add rdx, '0'
     dec rsp
     mov [rsp], dl
     test rax, rax
     jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, rbx
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
      mov al, byte[rdi]
      cmp al, byte[rsi]
      jnz .not_equal
      inc rdi
      inc rsi
      test al, al
      jne .loop
      mov rax, 1
      ret
    .not_equal:
    mov rax, 0
      ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    js .stop
    pop rax
    .stop:
      ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    .space_loop:
       call read_char
       cmp al, 0x20
       je .space_loop
       cmp al, 0x9
       je .space_loop
       cmp al, 0xA
       je .space_loop
    xor r14, r14
    dec r13
    .loop:
       cmp al, 0x20
       je .stop
       cmp al, 0x9
       je .stop
       cmp al, 0xA
       je .stop
       test al, al
       jz .stop
       cmp r14, r13
       jg .fail
       mov byte[r12+r14], al
       inc r14
       call read_char
       jmp .loop
    .success:
       mov byte[r12+r14], 0
       mov rax, r12
       mov rdx, r14
    .fail:
       xor rax, rax
    .stop:    
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov rsi, 10
    .loop:
       mov cl, byte[rdi+rcx]
       cmp cl, '0'
       jl .end
       cmp cl, '9'
       jg .end
       sub cl, '0'
       mul rsi
       add rax, rdx
       inc rcx
       jmp .loop
    .end:
    mov rdx, rcx   
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    jz .negative
    call parse_uint
    jmp .return
    .negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    .return:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
      cmp rax, rdx
      jge .buffer_not_fits
      mov r11b, byte[rdi+rax]
      mov byte[rsi+rax], r11b
    .buffer_not_fits:
       xor rax, rax
    .copy_finished:
      ret   
    ret
